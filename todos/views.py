from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
from django.http import HttpRequest, HttpResponse


# Create your views here.
def todolist(request: HttpRequest):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request: HttpRequest, id: int):
    # todo_list = TodoList.objects.get(id=id)
    todo_list = get_object_or_404(TodoList, id=id)
    # tasks = todo_list.items.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoListForm(instance=edit)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    edit = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.list.id)
    else:
        form = TodoItemForm(instance=edit)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
